<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CalculatorController extends Controller
{
    public function index(Request $request)
    {
            $length = $request->input('length');
            $width = $request->input('width');
            $height = $request->input('height');
            $formula = $request->input('formula');
            $result = 0;
            $method;
            if ($formula == '1') {
            	$result = ($length + $width) * 2;
                $method = "The perimeter of a rectangle is ";
            }elseif ($formula == '2') {
            	$result = $width * $height * $length;
                $method = "The volume of cuboid is ";
            }elseif($formula =='3'){
                $result = $length * $length;
                $method = "The area of square is ";
            }else{
            	$result = 0;
            }
            //echo $result;
            return redirect('/')->with('message', "$method".$result);
    }
}
