<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Calculator</title>
        <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" >
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    </head>
    <body style="background-color: gray;">
          <div class="container text-center">
              <div class="row">
                  <div class="col-md-4 m-auto">
                    <form action="calculation" method="POST">
                        @csrf
                        <div class="card" style="background-color: black; margin-top: 50px;">
                          <div class="card-body m-auto">
                              <h2 class="text-center text-light" style="margin: 5px; margin-bottom: 40px;">Midterm Calculator</h2>

                              <div class="form-group row">
                                  <div class="col-md-12">
                                      <select name="formula" class="form-control">
                                          <option>----Select Formula----</option>
                                          <option value="1">Perimeter of a Rectangle</option>
                                          <option value="2">The volume of cuboid</option>
                                          <option value="3">Area of square</option>
                                      </select>
                                  </div>
                              </div>

                              <div class="form-group row">
                                  <div class="col-md-12">
                                      <input type="number" name="length" class="form-control" placeholder="Enter length">
                                  </div>
                              </div>

                               <div class="form-group row">
                                  <div class="col-md-12">
                                      <input type="number" name="width" class="form-control" placeholder="Enter width">
                                  </div>
                               </div>

                               <div class="form-group row">
                                  <div class="col-md-12">
                                      <input type="number" name="height" class="form-control" placeholder="Enter height">
                                  </div>
                               </div>
                    
                          </div>
                          <div class="container" style="padding-bottom: 30px;">
                            <input type="submit" name="btn" class="btn btn-primary btn-lg font-weight-bold" value="Enter">
                          </div>
                      </div>
                    </form>
                  </div>
              </div>
          </div> 
          <div class="container" style="margin-top: 20px;">
              <div class="col-md-4 m-auto">
                  @if(session('message'))
                  <div class="alert alert-warning">
                      <h1 class="text-center">{{ session('message') }}</h1>
                  </div>
                  @endif
              </div>
          </div>
    </body>
</html>
